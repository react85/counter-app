import ReactDOM from 'react-dom';
import CounterApp from './CounterApp';
// import FirstApp from './FirstApp';
import './index.css';

const divRoot = document.querySelector('#root');

// ReactDOM.render(<FirstApp title="Hello, my name is David" />, divRoot);
ReactDOM.render(<CounterApp value={123} />, divRoot);

import { useState } from 'react';
import PropTypes from 'prop-types';

const CounterApp = ({ value }) => {

    /** Hook useState format
     * const useState = (value) => {
     *      return [value, () => {console.log('Hello world')}]
     * };
     */

    const [counter, setCounter] = useState(value);


    const handleAdd = () => setCounter((c) => c + 1);
    // setCounter(counter + 1);

    return (
        <>
            <h1>CounterApp</h1>
            <h2>{counter}</h2>

            <button onClick={handleAdd}>+1</button>
            <button onClick={() => setCounter(value)}>Reset</button>
            <button onClick={() => setCounter((c) => c - 1)}>-1</button>
        </>
    );
};

CounterApp.propTypes = {
    value: PropTypes.number.isRequired
};

export default CounterApp;
import { getSaludo } from "../base/02-template-string";

describe('Test on 02-template-string.js', () => {
    test('should return hello plus name', () => {
        const name = 'David';

        const saludo = getSaludo(name);

        expect(saludo).toBe(`Hello ${name}`);
    })

})

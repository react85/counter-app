import '@testing-library/jest-dom';
import { getUser, getUsuarioActivo } from '../base/05-funciones';

describe('Test on 05-funciones', () => {
    test('should return an object', () => {
        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        };

        const user = getUser();

        expect(user).toEqual(userTest);
    });

    test('should return an object when getUsuarioActivo is called', () => {
        const nameTest = 'David';
        const userTest = {
            uid: 'ABC567',
            username: nameTest
        };

        const user = getUsuarioActivo(nameTest);

        expect(user).toEqual(userTest);
    });


})

import { shallow } from 'enzyme';
import CounterApp from '../CounterApp';

describe('Test on <CounterApp />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<CounterApp value={1} />);
    });

    test('should show <CounterApp /> correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('should show the value 100 when is called with value 100', () => {
        const value = 100;

        const wrapper = shallow(<CounterApp value={value} />);
        const counterText = wrapper.find('h2').text().trim();

        expect(counterText).toBe(value.toString());
    });

    test('should increment with the +1 button', () => {

        wrapper.find('button').at(0).simulate('click');

        const counterText = wrapper.find('h2').text().trim();

        expect(counterText).toBe('2');

    });

    test('should decrement with the -1 button', () => {

        wrapper.find('button').at(2).simulate('click');

        const counterText = wrapper.find('h2').text().trim();

        expect(counterText).toBe('0');

    });

    test('should reset value with reset button', () => {

        wrapper.find('button').at(0).simulate('click').simulate('click');
        wrapper.find('button').at(1).simulate('click');

        const counterText = wrapper.find('h2').text().trim();

        expect(counterText).toBe('1');

    });


});

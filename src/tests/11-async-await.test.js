import { getImagen } from '../base/11-async-await';

describe('Test with async-await and fetch', () => {
    test('should return gif url', async () => {
        const url = await getImagen();
        console.log(url);
        expect(typeof url).toBe('string');
        expect(url.includes('https://')).toBe(true);
    });
});

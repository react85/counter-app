import { getHeroeByIdAsync } from "../base/09-promesas";
import heroes from "../data/heroes";

describe('Promises tests', () => {
    test('getheroeByIdAsync should return a hero async', (done) => {
        const id = 1;
        getHeroeByIdAsync(id)
            .then(hero => {
                expect(hero).toBe(heroes[0]);
                done();
            });

    });

    test('getheroeByIdAsync should return error if hero by id does not exist', (done) => {
        const id = 21;
        getHeroeByIdAsync(id)
            .catch(error => {
                expect(error).toBe('No se pudo encontrar el héroe');
                done();
            });

    });

});

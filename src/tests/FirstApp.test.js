import { shallow } from 'enzyme';
import FirstApp from '../FirstApp';


describe('Tests on <FirstApp />', () => {

    test('should show <FirstApp /> correctly', () => {
        const title = 'Hi! I\'m David';
        const wrapper = shallow(<FirstApp title={title} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('should show the subtitle sended by props', () => {
        const title = 'Hi! I\'m David';
        const subtitle = 'Hi! I\'m the subtitle from David';
        const wrapper = shallow(<FirstApp title={title} subtitle={subtitle} />);

        const paragraphText = wrapper.find('p').text();

        expect(paragraphText).toBe(subtitle);
    });


});

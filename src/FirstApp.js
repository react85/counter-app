import React from 'react';
import PropTypes from 'prop-types';

const FirstApp = ({ title, subtitle }) => {
    // const title = 'Hello world';
    // console.log(props);

    return (
        <>
            <h1> {title}</h1>
            {/* <pre>{JSON.stringify(title, null, 3)}</pre> */}
            <p>{subtitle}</p>
        </>
    );
};

FirstApp.propTypes = {
    title: PropTypes.string.isRequired
};

FirstApp.defaultProps = {
    subtitle: 'I\'m a subtitle'
};

export default FirstApp;